import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ModalService {

  showRegistrationModal = false;
  showAddingSystemModal = false;

  constructor() { }
}
