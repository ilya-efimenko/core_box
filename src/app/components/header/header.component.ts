import { Component, OnInit } from '@angular/core';
import { ModalService } from '../../services/modals/modal.service'

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  userName;

  constructor(
    public modalService: ModalService
  ) { }

  ngOnInit(): void {
    localStorage.getItem('userName') ? this.userName = localStorage.getItem('userName') : this.userName = 'Login';
  }
}
