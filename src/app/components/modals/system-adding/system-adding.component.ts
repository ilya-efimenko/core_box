import { Component, OnInit } from '@angular/core';
import { ModalService } from '../../../services/modals/modal.service';

@Component({
  selector: 'app-system-adding',
  templateUrl: './system-adding.component.html',
  styleUrls: ['./system-adding.component.scss']
})
export class SystemAddingComponent implements OnInit {

  constructor(
    public modalService: ModalService
  ) { }

  ngOnInit(): void {
  }

}
