import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SystemAddingComponent } from './system-adding.component';

describe('SystemAddingComponent', () => {
  let component: SystemAddingComponent;
  let fixture: ComponentFixture<SystemAddingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SystemAddingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SystemAddingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
