import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SystemPlusComponent } from './system-plus.component';

describe('SystemPlusComponent', () => {
  let component: SystemPlusComponent;
  let fixture: ComponentFixture<SystemPlusComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SystemPlusComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SystemPlusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
