import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { ModalService } from '../../../services/modals/modal.service';

import { SocialAuthService } from "angularx-social-login";
import { GoogleLoginProvider } from "angularx-social-login";
import { SocialUser } from "angularx-social-login";

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent implements OnInit {

  errorText: any;
  user: SocialUser;
  loggedIn: boolean;

  userForm: FormGroup = new FormGroup({
    name: new FormControl(''),
    email: new FormControl(''),
    password: new FormControl('')
  });

  constructor(
    public formBuilder: FormBuilder,
    public modalService: ModalService,
    private router: Router,
    private authService: SocialAuthService
  ) { }

  ngOnInit(): void {
    this.initForm();
    this.authService.authState.subscribe((user) => {
      localStorage.setItem('userName', user.name);
      this.user = user;
      this.loggedIn = (user != null);
    });
  }

  initForm(): void {
    this.userForm = this.formBuilder.group({
      name: ['', Validators.required],
      email: ['', Validators.compose([Validators.required, this.validInputs()])],
      password: ['', Validators.compose([Validators.required, this.validInputs()])]
    }, { updateOn: 'change' });
  }

  onSubmit(userData: any): void {
    localStorage.setItem('coreBoxUserId', `_${Math.random().toString(36).substr(2, 9)}`);
    localStorage.setItem('userName', userData.name);
  }

  validInputs() {
    return (control: FormControl) => {
      const regExpEmail = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      const regExpPassword = /(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/;
      if (regExpEmail.test(control.value) || regExpPassword.test(control.value)) {
        return null;
      } else {
        return this.errorText = {
          restrictedTextEmail: 'Email is invalid.',
          restrictedTextPassword: 'Password is invalid.'
        };
      }
    };
  }

  signInWithGoogle(): void {
    this.authService.signIn(GoogleLoginProvider.PROVIDER_ID).then((resp) => {
      this.modalService.showRegistrationModal = false;
      this.router.navigate(['/systems']);
    }
    );
  }

  signOut(): void {
    this.authService.signOut();
  }

  refreshToken(): void {
    this.authService.refreshAuthToken(GoogleLoginProvider.PROVIDER_ID);
  }
}
