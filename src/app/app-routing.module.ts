import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { MainComponent } from './pages/main/main.component';
import { UserSystemsComponent } from './pages/user-systems/user-systems.component';

import { LoginGuard } from './guards/login/login.guard';
import { RegistrationGuard } from './guards/registration/registration.guard';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'systems',
    pathMatch: 'full'
  },
  {
    path: 'main',
    component: MainComponent
    // canActivate: [RegistrationGuard]
  },
  {
    path: 'systems',
    component: UserSystemsComponent,
    canActivate: [LoginGuard]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
