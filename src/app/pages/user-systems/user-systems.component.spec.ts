import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserSystemsComponent } from './user-systems.component';

describe('UserSystemsComponent', () => {
  let component: UserSystemsComponent;
  let fixture: ComponentFixture<UserSystemsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UserSystemsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UserSystemsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
