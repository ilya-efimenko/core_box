import { Component, OnInit } from '@angular/core';
import { ModalService } from '../../services/modals/modal.service';

@Component({
  selector: 'app-user-systems',
  templateUrl: './user-systems.component.html',
  styleUrls: ['./user-systems.component.scss']
})
export class UserSystemsComponent implements OnInit {

  constructor(
    public modalService: ModalService
  ) { }

  ngOnInit(): void {
  }

}
