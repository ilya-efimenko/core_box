import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { SocialLoginModule, SocialAuthServiceConfig } from 'angularx-social-login';
import { GoogleLoginProvider } from 'angularx-social-login';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MainComponent } from './pages/main/main.component';
import { UserSystemsComponent } from './pages/user-systems/user-systems.component';
import { SystemModuleComponent } from './components/system-module/system-module.component';
import { SystemComponent } from './components/system/system.component';
import { HeaderComponent } from './components/header/header.component';
import { RegistrationComponent } from './components/modals/registration/registration.component';
import { SystemAddingComponent } from './components/modals/system-adding/system-adding.component';
import { ModuleAddingComponent } from './components/modals/module-adding/module-adding.component';
import { SystemPlusComponent } from './components/modals/system-plus/system-plus.component';
import { SliderComponent } from './components/slider/slider.component';
import { LandingComponent } from './pages/landing/landing.component';

@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    UserSystemsComponent,
    SystemModuleComponent,
    SystemComponent,
    HeaderComponent,
    RegistrationComponent,
    SystemAddingComponent,
    ModuleAddingComponent,
    SystemPlusComponent,
    SliderComponent,
    LandingComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    SocialLoginModule
  ],
  providers: [
    {
      provide: 'SocialAuthServiceConfig',
      useValue: {
        autoLogin: false,
        providers: [
          {
            id: GoogleLoginProvider.PROVIDER_ID,
            provider: new GoogleLoginProvider(
              '1000838056819-eqaj3c4sqe15k6gu8qusqsbfdso9riau.apps.googleusercontent.com'
            )
          }
        ]
      } as SocialAuthServiceConfig,
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
